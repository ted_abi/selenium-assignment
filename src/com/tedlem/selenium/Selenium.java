package com.tedlem.selenium;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
//domain ,data, controll,
public class Selenium {
	
	WebDriver driver;
	
	private String password = "Enter password here";
	
	public WebDriver setUp() {
		
		System.setProperty("webdriver.gecko.driver", "/usr/local/share/gecko_driver/geckodriver");
		
		driver = new FirefoxDriver();  
		return driver;
		
	}
	
	public static void main(String[] args) {
		
		Selenium sel = new Selenium();
		try {
			
		sel.portal();
		
		//sel.mail();
		
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}
		
	//for the portal
	public void portal() throws Exception {
		
		driver = setUp();
		
		driver.get("http://portal.aait.edu.et");
		
	    WebElement username = driver.findElement(By.id("UserName"));
		WebElement password = driver.findElement(By.id("Password"));
		WebElement button = driver.findElement(By.cssSelector(".btn.btn-success"));
		
		username.sendKeys("atr/5433/09");
		password.sendKeys(" Enter password here ");
		button.click();
		// Print a Log In message to the screen
		System.out.println("Successfully opened the website localhost");
		
		
		WebElement gradeAndResult = driver.findElement(By.id("ml2"));
		Thread.sleep(4000);
		
		gradeAndResult.click();
		
		// retrive table
		List<WebElement> tablerow = driver.findElements(By.tagName("tr"));
		
		File file = new File("data.txt");
		FileWriter fileWrite = new FileWriter(file,true);
		BufferedWriter printWriter = new BufferedWriter(fileWrite);
		
		if(printWriter!=null) {
			System.out.println("the file has been created! ");
		}
		
		for(int i=1;i<26;i++) {
			List<WebElement> colvalue = tablerow.get(i).findElements(By.tagName("td"));
			if(colvalue.size() > 1) {
				for(int j=0; j<7;j++) {
					System.out.println(colvalue.get(j).getText());
					printWriter.append(colvalue.get(j).getText()+"\n");
					printWriter.append("");
				}	
			}
			
		}
		//Wait for some time (longer in my case)
		printWriter.close();
		Thread.sleep(299000);	
		
		// Close the driver
		driver.quit();
	}

	
	//for the gmail
	public void mail() throws Exception {
			
		driver = setUp();
		String url = "https://mail.google.com/mail/u/0/#inbox";
		
		// navigate to gmail website 
		driver.navigate().to(url); 
			
		// enter email address
		WebElement email = driver.findElement(By.xpath("//input[@name='identifier']")); 
		WebElement sendbttn = driver.findElement(By.xpath("//*[@id=\"identifierNext\"]"));
		    
		email.sendKeys("email@gmail.com");
		sendbttn.click();
			
		Thread.sleep(5000);
		    
		WebElement passwordv =  driver.findElement(By.cssSelector("input[name='password']"));
		    
		passwordv.click();
		    
		passwordv.sendKeys(password); 
		    
		driver.findElement(By.xpath("//*[@id=\"passwordNext\"]")).click(); // submit form
		
		Thread.sleep(3000);
		List<WebElement> unread = driver.findElements(By.xpath("//*[@class='zF']"));
		
		// write on file
		FileWriter fw=new FileWriter("unread_messages.txt");
		   
		for(int i=0;i<unread.size();i++) {
			System.out.println(unread.get(i).getText()+"\n");
	    	
	        if ( unread.get(i).isDisplayed()) {

	        	//System.out.println(unread.get(i).findElement(By.cssSelector(".bqX.brq")));
	            System.out.println("unread messages list");
	                    
	            WebElement headerElement = unread.get(i).findElement(By.xpath("//span[@class='bqe']"));
	            WebElement bodyElement = unread.get(i).findElement(By.xpath("//span[@class='y2']"));

	            //headerElement.getAttribute()
	            System.out.println(headerElement.getTagName());

	            fw.append("from : ").append(unread.get(i).getText()).append("\n");
	            fw.append("head : ").append(headerElement.getText()).append("\n");
	            fw.append("body : ").append(bodyElement.getText()).append("\n");

	            fw.append("                 ");
	         }
		}
		    
		fw.close();
		Thread.sleep(10000);
		driver.quit();
		}	
}
